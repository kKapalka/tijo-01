/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author student
 */
public class JavaApplication2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final float CIRCLE_R = 3.0F;
        final float RECTANGLE_WIDTH = 3.0F;
        final float RECTANGLE_HEIGHT = 4.0F;
// parametry konstruktora: szerokosc i wysokosc prostokata
        final Shape rectangle = new Rectangle(RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
        System.out.println("Pole powierzchni prostokata: " + rectangle.getArea());
        System.out.println("Obwod prostokata: " + rectangle.getCircuit());
// parametr konstruktora: promien
        final Shape circle = new Circle(CIRCLE_R);
        System.out.println("Pole powierzchni kola: " + circle.getArea());
        System.out.println("Obwod kola: " + circle.getCircuit());
    }

}
