/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author student
 */
public class Rectangle extends Shape{
    
    public Rectangle(float width, float height){
        this.width = width;
        this.height = height;
    }
    
    private float width;
    private float height;
    
    // pole powierzchni figury
    public float getArea(){
        float area = (float)(this.width * this.height);
        return area;
    }
    // obwod figury
    public float getCircuit(){
        float circuit = (float)(2 * this.height + 2 * this.width);
        return circuit;
    }
}
