/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author student
 */
public class Circle extends Shape{
    
    private float r;
    
    public Circle (float r){
        this.r = r;
    }
    
    @Override
    public float getArea(){
        float area = (float)(Math.PI * this.r * this.r);
        return area;
    }
    
    @Override
    public float getCircuit(){
        float circuit = (float)(2 * Math.PI * this.r);
        return circuit;
    }
}
