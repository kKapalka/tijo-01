/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

/**
 *
 * @author student
 */
public abstract class Shape {
    
    // pole powierzchni figury
    public abstract float getArea();
    // obwod figury
    public abstract float getCircuit();
}
