/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

class EmptyDataException extends Exception {
}

/**
 *
 * @author student
 */
public class JavaApplication1 {

    private int getMaxFromDigits(final String digits) throws EmptyDataException {
        if (digits == null || digits.isEmpty()) {
            throw new EmptyDataException();
        }
        
        String[] parts = digits.split(",");
        int max = Integer.parseInt(parts[0]);
        for (String liczba : parts) {
            int number = Integer.parseInt(liczba);
            if (number > max) {
                max = number;
            }
        }
        //System.out.println(part1);
        return max;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String listOfDigits = null;
        final JavaApplication1 main = new JavaApplication1();
        try {
            int maxFromDigits = main.getMaxFromDigits(listOfDigits);
            System.out.printf("max(%s) = %d \n", listOfDigits, maxFromDigits);
        } catch (EmptyDataException e) {
            System.out.println("Lista nie moze byc pusta!");
        }
    }

}
